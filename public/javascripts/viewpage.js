﻿function create(boxes, results) {
    if (boxes === "NONE") {
        var para = document.createElement('h3');
        var appender = document.getElementById('begin');
        var node = document.createTextNode("There are currently no ECO changes logged.");
        para.appendChild(node);
        appender.appendChild(para);
    }
    else{
        for (var i = 0; i < boxes.length; i++) {
            var original = document.getElementById("accordion");
            var panel = document.createElement('div');
            var heading = document.createElement('div');
            var title = document.createElement('h4');
            var link = document.createElement('a');
            var collapse = document.createElement('div');
            var body = document.createElement('div');
            
            link.setAttribute("data-toggle", "collapse");
            link.setAttribute("data-target", "#collapse" + boxes[i]);
            link.setAttribute("href", "#collapse" + boxes[i]);
            if (boxes[i] === 0)
                link.text = "Changes without a Revision Number";
            else
                link.text = "Revision " + boxes[i];
            title.appendChild(link);
            //function should be here instead
            body.appendChild(makeBody(results, boxes[i]));
            //end
            title.className = "panel-title";
            body.className = "panel-body";

            heading.appendChild(title);
            collapse.appendChild(body);
            collapse.id = "collapse" + boxes[i];
            collapse.className = "panel-collapse collapse";
            heading.className = "panel-heading";
            panel.appendChild(heading);
            panel.appendChild(collapse);
            panel.className = "panel panel-default";
            panel.id = "panel" + boxes[i];
            
            original.appendChild(panel);
        }
    }
}

function makeBody(results, revNum) {
    if(revNum === 0) {
        var form = document.createElement('form');
        var button = document.createElement('button');
        var body = document.createElement('tbody');
        var table = document.createElement('table');
        var headtr = document.createElement('tr');
        var headtdBox = document.createElement('td');
        var headtd1 = document.createElement('td');
        var headtd2 = document.createElement('td');
        var headtd3 = document.createElement('td');
        
        headtdBox.textContent = "";
        headtd1.textContent = "CHANGE LOCATION";
        headtd2.textContent = "CHANGE TYPE";
        headtd3.textContent = "DETAILS";
        headtr.appendChild(headtdBox);
        headtr.appendChild(headtd1);
        headtr.appendChild(headtd2);
        headtr.appendChild(headtd3);
        body.appendChild(headtr);
        
        for (var i = 0; i < results.length; i++) {
            if (results[i].revNum === revNum) {
                var nexttr = document.createElement('tr');
                var nexttdBox = document.createElement('td');
                var nexttd1 = document.createElement('td');
                var nexttd2 = document.createElement('td');
                var nexttd3 = document.createElement('td');
                var checked = document.createElement('input');
                checked.id = "check" + i;
                checked.type = "checkbox";
                checked.name = "checkbox";
                nexttdBox.appendChild(checked);
                nexttd1.textContent = results[i].changeLoc;
                nexttd2.textContent = results[i].changeType;
                nexttd3.textContent = results[i].description;
                nexttr.appendChild(nexttdBox);
                nexttr.appendChild(nexttd1);
                nexttr.appendChild(nexttd2);
                nexttr.appendChild(nexttd3);
                nexttr.id = results[i].ecoid;
                body.appendChild(nexttr);
            }
        }
        
        button.setAttribute("type", "button");
        button.className = "btn btn-primary";
        button.textContent = "Add Selected Changes to a Revision";
        
        table.appendChild(body);
        table.style.width = "100%";
        table.className = "table table-striped";
        table.id = "table" + revNum;
        
        form.appendChild(table);
        form.appendChild(button);
        
        button.addEventListener('click', function () { addRevNum(); }, false);
        
        return form;
    }
    else {
        var body = document.createElement('tbody');
        var table = document.createElement('table');
        var headtr = document.createElement('tr');
        var headtd1 = document.createElement('td');
        var headtd2 = document.createElement('td');
        var headtd3 = document.createElement('td');
        
        headtd1.textContent = "CHANGE LOCATION";
        headtd2.textContent = "CHANGE TYPE";
        headtd3.textContent = "DETAILS";
        headtr.appendChild(headtd1);
        headtr.appendChild(headtd2);
        headtr.appendChild(headtd3);
        body.appendChild(headtr);
        
        for (var i = 0; i < results.length; i++) {
            if (results[i].revNum === revNum) {
                var nexttr = document.createElement('tr');
                var nexttd1 = document.createElement('td');
                var nexttd2 = document.createElement('td');
                var nexttd3 = document.createElement('td');
                nexttd1.textContent = results[i].changeLoc;
                nexttd2.textContent = results[i].changeType;
                nexttd3.textContent = results[i].description;
                nexttr.appendChild(nexttd1);
                nexttr.appendChild(nexttd2);
                nexttr.appendChild(nexttd3);
                body.appendChild(nexttr);
            }
        }
        
        table.appendChild(body);
        table.style.width = "100%";
        table.className = "table table-striped";
        table.id = "table" + revNum;

        return table;
    }
    //----------------------------------------------------------------------------
}

function addRevNum() {
    var url = document.URL;
    var mainTable = document.getElementById("table0");
    var trlist = mainTable.getElementsByTagName('tr');
    var checklist = document.getElementsByName('checkbox');
    var info = [];
    var formToSubmit = document.createElement('form');
    var output = document.createElement('input');
    var submitRevNum = document.createElement('input');
    var newRevNum = prompt("Please enter the Revision Number");
    if (newRevNum != null) {
        for (var i = 0; i < checklist.length; i++) {
            if (checklist[i].checked === true) {
                info.push(checklist[i].parentElement.parentElement.id);
            }
        }
        var realNum = Number(newRevNum);
        var data = JSON.stringify(info);
        var num = url.indexOf('view/') + 5;
        var wholenumber = url.substring(num);
        output.type = 'text';
        output.setAttribute("value", data);
        output.setAttribute("name", "idPasser");
        submitRevNum.type = 'text';
        submitRevNum.setAttribute("value", realNum);
        submitRevNum.setAttribute("name", "revPasser");
        formToSubmit.appendChild(output);
        formToSubmit.appendChild(submitRevNum);
        formToSubmit.action = "/write/changerev/" + wholenumber;
        formToSubmit.method = 'post';
        formToSubmit.enctype = "multipart/form-data";
        formToSubmit.submit();
    }
}

$(function () {
    create(boxes, results);
});