﻿var i = [0];

function increment() {
    i.push(i[i.length - 1]+1);
}

function changeSelectLoc(index) {
    if ($("#row" + index).children('td').children('select').children('option').filter(':selected')[0].text == "Bom") {
        var row = $("#row"+index)[0];

        $("#row"+index)[0].innerHTML = '';
        
        var tdpos1 = document.createElement('td');
        var tdpos2 = document.createElement('td');
        var tdposIcon = document.createElement('td');
        var glyphicon = document.createElement('span');
        var changeLoc = document.createElement('select');
        var optionBom = document.createElement('option');
        var optionNetlist = document.createElement('option');
        var changeType = document.createElement('select');
        var optionAdd = document.createElement('option');
        var optionChange = document.createElement('option');
        var optionDefault = document.createElement('option');
        
        optionBom.text = 'Bom';
        optionNetlist.text = 'Netlist';
        optionDefault.text = "-- Change Type --";
        optionAdd.text = 'Add';
        optionChange.text = 'Change';
        optionDefault.disabled = true;
        optionDefault.selected = true;
        
        changeLoc.appendChild(optionBom);
        changeLoc.appendChild(optionNetlist);
        changeType.appendChild(optionDefault);
        changeType.appendChild(optionAdd);
        changeType.appendChild(optionChange);
        
        glyphicon.className = 'glyphicon glyphicon-remove';
        glyphicon.style.marginLeft = '10px';
        changeLoc.name = 'changeLoc';
        changeLoc.className = 'form-control';
        changeType.name = 'changeType';
        changeType.className = 'form-control';
        
        tdpos1.appendChild(changeLoc);
        tdpos2.appendChild(changeType);
        tdposIcon.appendChild(glyphicon);
        
        row.appendChild(tdpos1);
        row.appendChild(tdpos2);
        row.appendChild(tdposIcon);
        
        changeLoc.addEventListener('change', function () { changeSelectLoc(index); }, false);
        changeType.addEventListener('change', function () { changeChangeType(index); }, false);
        glyphicon.addEventListener('click', function () { rowRemove(index); }, false);
    }

    if ($("#row" + index).children('td').children('select').children('option').filter(':selected')[0].text == "Netlist") {
        var row = $("#row" + index)[0];
        
        $("#row" + index)[0].innerHTML = '';
        
        var tdpos1 = document.createElement('td');
        var tdpos2 = document.createElement('td');
        var tdposIcon = document.createElement('td');
        var glyphicon = document.createElement('span');
        var changeLoc = document.createElement('select');
        var optionBom = document.createElement('option');
        var optionNetlist = document.createElement('option');
        var changeType = document.createElement('select');
        var optionAdd = document.createElement('option');
        var optionRemove = document.createElement('option');
        var optionDefault = document.createElement('option');
        
        optionBom.text = 'Bom';
        optionNetlist.text = 'Netlist';
        optionDefault.text = "-- Change Type --";
        optionAdd.text = 'Add';
        optionRemove.text = 'Remove';
        optionDefault.disabled = true;
        optionDefault.selected = true;
        
        changeLoc.appendChild(optionNetlist);
        changeLoc.appendChild(optionBom);
        changeType.appendChild(optionDefault);
        changeType.appendChild(optionAdd);
        changeType.appendChild(optionRemove);
        
        glyphicon.className = 'glyphicon glyphicon-remove';
        glyphicon.style.marginLeft = '10px';
        changeLoc.name = 'changeLoc';
        changeLoc.className = 'form-control';
        changeType.name = 'changeType';
        changeType.className = 'form-control';
        
        tdpos1.appendChild(changeLoc);
        tdpos2.appendChild(changeType);
        tdposIcon.appendChild(glyphicon);
        
        row.appendChild(tdpos1);
        row.appendChild(tdpos2);
        row.appendChild(tdposIcon);

        changeLoc.addEventListener('change', function () { changeSelectLoc(index); }, false);
        changeType.addEventListener('change', function () { changeChangeType(index); }, false);
        glyphicon.addEventListener('click', function () { rowRemove(index); }, false);
    }
}



function changeChangeType(index) {
    if ($("#row" + index).children('td').children('select').children('option').filter(':selected')[0].text == "Bom") {
        if ($("#row" + index).children('td').children('select').children('option').filter(':selected')[1].text == "Add") {
            var row = $("#row" + index)[0];
            
            $("#row" + index)[0].innerHTML = '';
            
            var tdpos1 = document.createElement('td');
            var tdpos2 = document.createElement('td');
            var tdRef = document.createElement('td');
            var tdVal = document.createElement('td');
            var tdposIcon = document.createElement('td');
            var glyphicon = document.createElement('span');
            var changeLoc = document.createElement('select');
            var optionBom = document.createElement('option');
            var optionNetlist = document.createElement('option');
            var changeType = document.createElement('select');
            var optionAdd = document.createElement('option');
            var optionChange = document.createElement('option');
            var inputRef = document.createElement('input');
            var inputVal = document.createElement('input');
            
            optionBom.text = 'Bom';
            optionNetlist.text = 'Netlist';
            optionAdd.text = 'Add';
            optionChange.text = 'Change';
            
            changeLoc.appendChild(optionBom);
            changeLoc.appendChild(optionNetlist);
            changeType.appendChild(optionAdd);
            changeType.appendChild(optionChange);
            
            glyphicon.className = 'glyphicon glyphicon-remove';
            glyphicon.style.marginLeft = '10px';
            changeLoc.name = 'changeLoc';
            changeLoc.className = 'form-control';
            changeType.name = 'changeType';
            changeType.className = 'form-control';
            inputRef.setAttribute('type', 'text');
            inputVal.setAttribute('type', 'text');
            inputRef.setAttribute('placeholder', "-- Part Reference --");
            inputVal.setAttribute('placeholder', "-- Value --");
            inputRef.setAttribute('required', 'true');
            inputVal.setAttribute('required', 'true');
            inputRef.className = 'form-control';
            inputVal.className = 'form-control';
            
            tdpos1.appendChild(changeLoc);
            tdpos2.appendChild(changeType);
            tdVal.appendChild(inputVal);
            tdRef.appendChild(inputRef);
            tdposIcon.appendChild(glyphicon);
            
            row.appendChild(tdpos1);
            row.appendChild(tdpos2);
            row.appendChild(tdVal);
            row.appendChild(tdRef);
            row.appendChild(tdposIcon);
            
            changeLoc.addEventListener('change', function () { changeSelectLoc(index); }, false);
            changeType.addEventListener('change', function () { changeChangeType(index); }, false);
            glyphicon.addEventListener('click', function () { rowRemove(index); }, false);
        }

        if ($("#row" + index).children('td').children('select').children('option').filter(':selected')[1].text == "Change") {
            var row = $("#row" + index)[0];
            
            $("#row" + index)[0].innerHTML = '';
            
            var tdpos1 = document.createElement('td');
            var tdpos2 = document.createElement('td');
            var tdRefOld = document.createElement('td');
            var tdValOld = document.createElement('td');
            var tdRefNew = document.createElement('td');
            var tdValNew = document.createElement('td');
            var tdposIcon = document.createElement('td');
            var glyphicon = document.createElement('span');
            var changeLoc = document.createElement('select');
            var optionBom = document.createElement('option');
            var optionNetlist = document.createElement('option');
            var changeType = document.createElement('select');
            var optionAdd = document.createElement('option');
            var optionChange = document.createElement('option');
            var inputRefOld = document.createElement('input');
            var inputValOld = document.createElement('input');
            var inputRefNew = document.createElement('input');
            var inputValNew = document.createElement('input');
            
            optionBom.text = 'Bom';
            optionNetlist.text = 'Netlist';
            optionAdd.text = 'Add';
            optionChange.text = 'Change';
            
            changeLoc.appendChild(optionBom);
            changeLoc.appendChild(optionNetlist);
            changeType.appendChild(optionChange);
            changeType.appendChild(optionAdd);
            
            glyphicon.className = 'glyphicon glyphicon-remove';
            glyphicon.style.marginLeft = '10px';
            changeLoc.name = 'changeLoc';
            changeLoc.className = 'form-control';
            changeType.name = 'changeType';
            changeType.className = 'form-control';
            inputRefOld.setAttribute('type', 'text');
            inputValOld.setAttribute('type', 'text');
            inputRefOld.setAttribute('placeholder', "-- Current Part Reference --");
            inputValOld.setAttribute('placeholder', "-- Current Value --");
            inputRefOld.className = 'form-control';
            inputValOld.className = 'form-control';
            inputRefNew.setAttribute('type', 'text');
            inputValNew.setAttribute('type', 'text');
            inputRefNew.setAttribute('placeholder', "-- New Part Reference --");
            inputValNew.setAttribute('placeholder', "-- New Value --");
            inputRefNew.className = 'form-control';
            inputValNew.className = 'form-control';
            
            tdpos1.appendChild(changeLoc);
            tdpos2.appendChild(changeType);
            tdValOld.appendChild(inputValOld);
            tdRefOld.appendChild(inputRefOld);
            tdValNew.appendChild(inputValNew);
            tdRefNew.appendChild(inputRefNew);
            tdposIcon.appendChild(glyphicon);
            
            row.appendChild(tdpos1);
            row.appendChild(tdpos2);
            row.appendChild(tdValOld);
            row.appendChild(tdRefOld);
            row.appendChild(tdValNew);
            row.appendChild(tdRefNew);
            row.appendChild(tdposIcon);
            
            changeLoc.addEventListener('change', function () { changeSelectLoc(index); }, false);
            changeType.addEventListener('change', function () { changeChangeType(index); }, false);
            glyphicon.addEventListener('click', function () { rowRemove(index); }, false);
        }
    }

    if ($("#row" + index).children('td').children('select').children('option').filter(':selected')[0].text == "Netlist") {
        if ($("#row" + index).children('td').children('select').children('option').filter(':selected')[1].text == "Remove") {
            var row = $("#row" + index)[0];
            
            $("#row" + index)[0].innerHTML = '';
            
            var tdpos1 = document.createElement('td');
            var tdpos2 = document.createElement('td');
            var tdNet1 = document.createElement('td');
            var tdNode1 = document.createElement('td');
            var tdRef1 = document.createElement('td');
            var tdPin1 = document.createElement('td');
            var tdposIcon = document.createElement('td');
            var glyphicon = document.createElement('span');
            var changeLoc = document.createElement('select');
            var optionBom = document.createElement('option');
            var optionNetlist = document.createElement('option');
            var changeType = document.createElement('select');
            var optionAdd = document.createElement('option');
            var optionRemove = document.createElement('option');
            var inputNet1 = document.createElement('input');
            var inputNode1 = document.createElement('input');
            var inputRef1 = document.createElement('input');
            var inputPin1 = document.createElement('input');
            
            optionBom.text = 'Bom';
            optionNetlist.text = 'Netlist';
            optionRemove.text = 'Remove';
            optionAdd.text = 'Add';
            
            changeLoc.appendChild(optionNetlist);
            changeLoc.appendChild(optionBom);
            changeType.appendChild(optionRemove);
            changeType.appendChild(optionAdd);

            glyphicon.className = 'glyphicon glyphicon-remove';
            glyphicon.style.marginLeft = '10px';
            changeLoc.name = 'changeLoc';
            changeLoc.className = 'form-control';
            changeType.name = 'changeType';
            changeType.className = 'form-control';
            inputNet1.setAttribute('type', 'text');
            inputNode1.setAttribute('type', 'text');
            inputRef1.setAttribute('type', 'text');
            inputPin1.setAttribute('type', 'text');
            inputNet1.setAttribute('placeholder', "-- Net_Name --");
            inputNode1.setAttribute('placeholder', "-- Node_Name --");
            inputRef1.setAttribute('placeholder', "-- Reference --");
            inputPin1.setAttribute('placeholder', "-- Pin --");
            inputNet1.setAttribute('required', 'true');
            inputNode1.setAttribute('required', 'true');
            inputRef1.setAttribute('required', 'true');
            inputPin1.setAttribute('required', 'true');
            inputNet1.className = 'form-control';
            inputNode1.className = 'form-control';
            inputRef1.className = 'form-control';
            inputPin1.className = 'form-control';
            
            tdpos1.appendChild(changeLoc);
            tdpos2.appendChild(changeType);
            tdNet1.appendChild(inputNet1);
            tdNode1.appendChild(inputNode1);
            tdRef1.appendChild(inputRef1);
            tdPin1.appendChild(inputPin1);
            tdposIcon.appendChild(glyphicon);
            
            row.appendChild(tdpos1);
            row.appendChild(tdpos2);
            row.appendChild(tdNet1);
            row.appendChild(tdNode1);
            row.appendChild(tdRef1);
            row.appendChild(tdPin1);
            row.appendChild(tdposIcon);
            
            changeLoc.addEventListener('change', function () { changeSelectLoc(index); }, false);
            changeType.addEventListener('change', function () { changeChangeType(index); }, false);
            glyphicon.addEventListener('click', function () { rowRemove(index); }, false);
        }


        if ($("#row" + index).children('td').children('select').children('option').filter(':selected')[1].text == "Add") {
            var row = $("#row" + index)[0];
            
            $("#row" + index)[0].innerHTML = '';
            
            var tdpos1 = document.createElement('td');
            var tdpos2 = document.createElement('td');
            var tdNet = document.createElement('td');
            var tdNode = document.createElement('td');
            var tdRef = document.createElement('td');
            var tdPin = document.createElement('td');
            var tdposIcon = document.createElement('td');
            var glyphicon = document.createElement('span');
            var changeLoc = document.createElement('select');
            var optionBom = document.createElement('option');
            var optionNetlist = document.createElement('option');
            var changeType = document.createElement('select');
            var optionAdd = document.createElement('option');
            var optionRemove = document.createElement('option');
            var inputNet = document.createElement('input');
            var inputNode = document.createElement('input');
            var inputRef = document.createElement('input');
            var inputPin = document.createElement('input');
            
            optionBom.text = 'Bom';
            optionNetlist.text = 'Netlist';
            optionAdd.text = 'Add';
            optionRemove.text = 'Remove';
            
            changeLoc.appendChild(optionNetlist);
            changeLoc.appendChild(optionBom);
            changeType.appendChild(optionAdd);
            changeType.appendChild(optionRemove);
            glyphicon.className = 'glyphicon glyphicon-remove';
            glyphicon.style.marginLeft = '10px';
            changeLoc.name = 'changeLoc';
            changeLoc.className = 'form-control';
            changeType.name = 'changeType';
            changeType.className = 'form-control';
            inputNet.setAttribute('type', 'text');
            inputNode.setAttribute('type', 'text');
            inputRef.setAttribute('type', 'text');
            inputPin.setAttribute('type', 'text');
            inputNet.setAttribute('placeholder', "-- Net_Name --");
            inputNode.setAttribute('placeholder', "-- Node_Name --");
            inputRef.setAttribute('placeholder', "-- Reference --");
            inputPin.setAttribute('placeholder', "-- Pin --");
            inputNet.setAttribute('required', 'true');
            inputNode.setAttribute('required', 'true');
            inputRef.setAttribute('required', 'true');
            inputPin.setAttribute('required', 'true');
            inputNet.className = 'form-control';
            inputNode.className = 'form-control';
            inputRef.className = 'form-control';
            inputPin.className = 'form-control';
            
            tdpos1.appendChild(changeLoc);
            tdpos2.appendChild(changeType);
            tdNet.appendChild(inputNet);
            tdNode.appendChild(inputNode);
            tdRef.appendChild(inputRef);
            tdPin.appendChild(inputPin);
            tdposIcon.appendChild(glyphicon);
            
            row.appendChild(tdpos1);
            row.appendChild(tdpos2);
            row.appendChild(tdNet);
            row.appendChild(tdNode);
            row.appendChild(tdRef);
            row.appendChild(tdPin);
            row.appendChild(tdposIcon);
            
            changeLoc.addEventListener('change', function () { changeSelectLoc(index); }, false);
            changeType.addEventListener('change', function () { changeChangeType(index); }, false);
            glyphicon.addEventListener('click', function () { rowRemove(index); }, false);
        }
    }
}

function rowRemove(index) {
    var element = document.getElementById("formPanel"+index);
    element.parentNode.removeChild(element);
    i.splice(index, 1)
}

function rowAdd() {
    increment();
    var index = i[i.length - 1];
    
    var mainPanel = document.getElementById("mainPanel");
    //var addButton = $("#addButton")[0];
    var newPanel = document.createElement('div');
    var panelBody = document.createElement('div');
    var newForm = document.createElement('form');
    var newTable = document.createElement('table');
    var body = document.createElement('tbody');
    var newRow = document.createElement('tr');
    var tdpos1 = document.createElement('td');
    var tdposIcon = document.createElement('td');
    var glyphicon = document.createElement('span');
    var changeLoc = document.createElement('select');
    var optionDefault = document.createElement('option');
    var optionBom = document.createElement('option');
    var optionNetlist = document.createElement('option');
    
    optionBom.text = 'Bom';
    optionNetlist.text = 'Netlist';
    optionDefault.text = "-- Change Location --";
    optionDefault.disabled = true;
    optionDefault.selected = true;
    
    changeLoc.appendChild(optionDefault);
    changeLoc.appendChild(optionBom);
    changeLoc.appendChild(optionNetlist);
    
    glyphicon.className = 'glyphicon glyphicon-remove';
    glyphicon.style.marginLeft = '10px';
    changeLoc.name = 'changeLoc';
    changeLoc.className = 'form-control';
    
    tdpos1.appendChild(changeLoc);
    tdposIcon.appendChild(glyphicon);
    
    newRow.appendChild(tdpos1);
    newRow.appendChild(tdposIcon);
    
    newRow.id = "row" + index;
    newTable.appendChild(newRow);
    body.appendChild(newRow);
    newTable.appendChild(body);
    
    newTable.setAttribute('width', '100%');
    panelBody.appendChild(newTable);
    
    panelBody.className = "panel-body";
    panelBody.style.marginLeft = "10px";
    panelBody.style.marginRight = "10px";
    newPanel.appendChild(panelBody);
    
    newPanel.id = "formPanel" + index;
    newPanel.className = "panel panel-default";
    newPanel.style.marginTop = "10px";
    newPanel.style.marginLeft = "10px";
    newPanel.style.marginRight = "10px";
    //mainPanel.appendChild(newPanel);
    mainPanel.insertBefore(newPanel, mainPanel.children[i.length]);

    changeLoc.addEventListener('change', function () { changeSelectLoc(index); }, false);
    glyphicon.addEventListener('click', function () { rowRemove(index); }, false);
}

function submitForm(wholenumber) {
    var info = [];
    var confirmSubmit = confirm("The info will be submitted");
    var formToSubmit = document.createElement('form');
    var output = document.createElement('input');
    for (var index = 0; index < i.length; index++) {
        var infoData = {};
        info.push(infoData)
    }
    if (confirmSubmit) {
        for (var index = 0; index < i.length; index++) {
            
            var rowInfo = document.getElementById("row" + i[index]).children;

            for (var fields = 0; fields < rowInfo.length - 1; fields++) {
                switch (fields) {
                    case 0:
                        info[index].changeLoc = rowInfo[0].children[0].value;
                        break;
                    case 1:
                        info[index].changeType = rowInfo[1].children[0].value;
                        break;
                    case 2:
                        if (info[index].changeLoc === "Netlist")
                            info[index].netName = rowInfo[2].children[0].value;
                        else if (info[index].changeType === "Add")
                            info[index].value = rowInfo[2].children[0].value;
                        else
                            info[index].currentValue = rowInfo[2].children[0].value;
                        break;
                    case 3:
                        if (info[index].changeLoc === "Netlist")
                            info[index].nodeName = rowInfo[3].children[0].value;
                        else if (info[index].changeType === "Add")
                            info[index].refDes = rowInfo[3].children[0].value;
                        else
                            info[index].currentRefDes = rowInfo[3].children[0].value;
                        break;
                    case 4:
                        if (info[index].changeLoc === "Netlist")
                            info[index].refId = rowInfo[4].children[0].value;
                        else
                            info[index].newValue = rowInfo[4].children[0].value;
                        break;
                    case 5:
                        if (info[index].changeLoc === "Netlist")
                            info[index].pin = rowInfo[5].children[0].value;
                        else
                            info[index].newRefDes = rowInfo[5].children[0].value;
                        break;
                }
            }
        }
        
        var data = JSON.stringify(info);
        output.type = 'text';
        output.setAttribute("value", data);
        output.setAttribute("name", "dataPasser");
        //output.value = data;
        formToSubmit.appendChild(output);
        formToSubmit.action = "/write/inputeco/"+wholenumber;
        formToSubmit.method = 'post';
        formToSubmit.enctype = "multipart/form-data";
        formToSubmit.submit();
    }
}