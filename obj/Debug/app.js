﻿
/**
 * Module dependencies.
 */

var express = require('express');
var favicon = require('serve-favicon');
var logger = require('morgan');
var methodOverride = require('method-override');
var session = require('express-session');
var bodyParser = require('body-parser');
var multer = require('multer');
var errorHandler = require('errorhandler');
var mysql = require('mysql');
var routes = require('./routes');
var user = require('./routes/user');
var http = require('http');
var path = require('path');
var home = require('./routes/home');
var write = require('./routes/write.js');

var connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'tfowifd',
    database: 'scandb'
});

var app = express();

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(methodOverride());
app.use(session({
    resave: true,
    saveUninitialized: true,
    secret: 'uwotm8'
}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(multer());
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
    app.use(errorHandler());
}

connection.connect(function (err) {
    if (err) console.log("Could not connect to scandb");
    else {
        console.log("Connected!");
    }
});
connection.query("DROP TABLE IF EXISTS Units")
connection.query("DROP TABLE IF EXISTS Bom")
connection.query("DROP TABLE IF EXISTS Eco")
connection.query("DROP TABLE IF EXISTS Netlist")
connection.query("DROP TABLE IF EXISTS Basic")
connection.query("CREATE TABLE Basic(pnid INT PRIMARY KEY AUTO_INCREMENT,arena VARCHAR(25), createDate TIMESTAMP DEFAULT CURRENT_TIMESTAMP) ENGINE = INNODB;")
connection.query("CREATE TABLE Units(bid INT PRIMARY KEY AUTO_INCREMENT, basic_pnid INT, serialNumber VARCHAR(25), buildNumber VARCHAR(25), createDate TIMESTAMP DEFAULT CURRENT_TIMESTAMP )ENGINE = INNODB;")
connection.query("CREATE TABLE Bom(bomid INT PRIMARY KEY AUTO_INCREMENT,basic_pnid INT, value TEXT, refDes TEXT, createDate TIMESTAMP DEFAULT CURRENT_TIMESTAMP)ENGINE = INNODB;")
connection.query("CREATE TABLE Netlist(netid INT PRIMARY KEY AUTO_INCREMENT, basic_pnid INT, net TEXT, refId TEXT, pin TEXT, node TEXT, createDate TIMESTAMP DEFAULT CURRENT_TIMESTAMP)ENGINE = INNODB;")
connection.query("CREATE TABLE Eco(ecoid INT PRIMARY KEY AUTO_INCREMENT, basic_pnid INT, revNum INT, changeLoc TEXT, changeType TEXT, compDescription TEXT, userDescription TEXT, createDate TIMESTAMP DEFAULT CURRENT_TIMESTAMP)")
connection.query("ALTER TABLE Units ADD CONSTRAINT FK_Units FOREIGN KEY(basic_pnid) REFERENCES Basic(pnid) ON UPDATE CASCADE ON DELETE CASCADE;")
connection.query("ALTER TABLE Bom ADD CONSTRAINT FK_Bom FOREIGN KEY(basic_pnid) REFERENCES Basic(pnid) ON UPDATE CASCADE ON DELETE CASCADE;")
connection.query("ALTER TABLE Netlist ADD CONSTRAINT FK_Netlist FOREIGN KEY(basic_pnid) REFERENCES Basic(pnid) ON UPDATE CASCADE ON DELETE CASCADE;")
connection.query("ALTER TABLE Eco ADD CONSTRAINT FK_Eco FOREIGN KEY(basic_pnid) REFERENCES Basic(pnid) ON UPDATE CASCADE ON DELETE CASCADE;")

//BEGIN Routes
app.get('/', home.home);
app.get('/write', write.write);
app.get('/write/newnum/:id', write.newnum);
app.post('/write/finish', write.finish);
//END Routes

http.createServer(app).listen(app.get('port'), function () {
    console.log('Express server listening on port ' + app.get('port'));
});
