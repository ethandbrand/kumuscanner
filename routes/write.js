﻿var mysql = require('mysql');
var fs = require('fs');
var xlsx = require('node-xlsx');

exports.init = function (req, res) {
    var wholenumber = req.query.WholeNumber;
    var arenanumber = wholenumber.substring(0, 9);
    var serialnumber = wholenumber.substring(9);
    var connection = mysql.createConnection({
        host: 'localhost',
        user: 'root',
        password: 'tfowifd',
        database: 'scandb'
    });
    connection.connect(function (err) {
        if (err) console.log("Could not connect to scandb");
    else {
            console.log("Connected!");
        }
    });
    connection.query("SELECT arena FROM Basic WHERE arena = " + arenanumber, function (err, results) {
        var doesexist = results[0];
        connection.query("SELECT serialNumber FROM Units WHERE serialNumber = " + serialnumber, function (err, results) {
            var snexist = results[0];
            if (doesexist == undefined) {
                res.redirect("/write/newnum/" + wholenumber);
            }
                    else {
                if (snexist != undefined) {
                    res.redirect("/view/" + wholenumber);
                }
                        else {
                    res.redirect("/write/createserial/" + wholenumber);
                }
            }
        });

    });
    
}
    
exports.newnum = function (req, res) {
    var wholenumber = req.params.id;
    res.render('createpage', { pageData: { arena : '' + wholenumber.substring(0, 9), serial : '' + wholenumber.substring(9), wholenumber: wholenumber} });
}

exports.createserial = function (req, res) {
    var wholenumber = req.params.id;
    res.render('newserialpage', { pageData: { wholenumber : wholenumber }});
}

exports.addtounits = function (req, res) {
    var wholenumber = req.params.id;
    var buildnumber = req.body.buildNumber;
    var arenanumber = wholenumber.substring(0, 9);
    var serialnumber = wholenumber.substring(9);
    var connection = mysql.createConnection({
        host: 'localhost',
        user: 'root',
        password: 'tfowifd',
        database: 'scandb'
    });
    connection.connect(function (err) {
        if (err)
            console.log("Could not connect to scandb");
        else {
            console.log("Connected!");
            connection.query("SELECT pnid FROM Basic WHERE arena = " + arenanumber, function (err, results) {
                var basic_pnid = results[0].pnid;
                var post = { basic_pnid: basic_pnid, buildNumber: buildnumber, serialNumber: serialnumber };
                connection.query("INSERT INTO Units SET ?", post);
            });
        }
    });
    res.redirect("/");
}
    
exports.finish = function (req, res) {
    var arenanumber = req.body.arenaNumber;
    var serialnumber = req.body.serialNumber;
    var buildnumber = req.body.buildNumber;
    var wholenumber = "" + arenanumber + serialnumber;
    var connection = mysql.createConnection({
        host: 'localhost',
        user: 'root',
        password: 'tfowifd',
        database: 'scandb'
    });
    
    //console.log(req);
    var netlistBuffer = fs.readFileSync(req.files.netlist.path);
    var bomBuffer = xlsx.parse(fs.readFileSync(req.files.bom.path));
    console.log(bomBuffer);
    connection.connect(function (err) {
        if (err)
            console.log("Could not connect to scandb");
        else {
            console.log("Connected!");
            var query = connection.query("INSERT INTO Basic(arena) VALUES(" + arenanumber + ")");
            connection.query("SELECT pnid FROM Basic WHERE arena = " + arenanumber, function (err, results) {
                var basic_pnid = results[0].pnid;
                var post = { basic_pnid: basic_pnid, buildNumber: buildnumber, serialNumber: serialnumber };
                connection.query("INSERT INTO Units SET ?", post);
                connection.query("SELECT bid FROM Units WHERE serialNumber = " + serialnumber, function (err, results) {
                    var units_bid = results[0].bid;
                    var netlistString = netlistBuffer.toString('utf8');
                    var separatedNetlist = netlistString.split("\n");
                    var netname;
                    var refid;
                    var pin;
                    var nodename;
                    for (i = 0; i < separatedNetlist.length; i++) {
                        if (separatedNetlist[i].indexOf('NET_NAME') > -1)
                            netname = separatedNetlist[i + 1];
                        if (separatedNetlist[i].indexOf('NODE_NAME') > -1) {
                            var extraNetlistSection = separatedNetlist[i].split("\t");
                            nodename = extraNetlistSection[1].split(" ")[0];
                            pin = extraNetlistSection[1].split(" ")[1];
                        }
                        if (separatedNetlist[i].indexOf(':;') > -1) {
                            var extraNetlistSection = separatedNetlist[i].split(':;');
                            refid = extraNetlistSection[0];
                            var post = { basic_pnid: basic_pnid, units_bid: units_bid, net: netname, refId: refid, pin: pin, node: nodename };
                            connection.query("INSERT INTO Netlist SET ?", post);
                        }
                    }
                    for (i = 0; i < bomBuffer.worksheets[0].maxRow; i++) {
                        console.log(i);
                        if (bomBuffer.worksheets[0].data[i] != undefined) {
                            if (bomBuffer.worksheets[0].data[i][2] != undefined && bomBuffer.worksheets[0].data[i][2].value != "Value" && typeof bomBuffer.worksheets[0].data[i][5].value == "string") {
                                var value = bomBuffer.worksheets[0].data[i][2].value;
                                if (bomBuffer.worksheets[0].data[i][5] != undefined && bomBuffer.worksheets[0].data[i][5].value != "Part Reference" && typeof bomBuffer.worksheets[0].data[i][5].value == "string") {
                                    var refDes = bomBuffer.worksheets[0].data[i][5].value;
                                    var post = { basic_pnid: basic_pnid, units_bid: units_bid, value: value, refDes: refDes };
                                    connection.query("INSERT INTO Bom SET ?", post);
                                }
                            }
                        }
                    }
                });

                /*req.pipe(req.busboy);
                req.busboy.on('bom', function (fieldname, file, filename) {
                    console.log(file);
                });
                req.busboy.on('netlist', function (fieldname, file, filename) {
                    console.log(file);
                });*/

                //var buffer = fs.readFileSync(netpath);
                //console.log(buffer);
                /* fs.open(netpath, 'r', function (err, fd) {
                    if (err) throw err;
                    fs.fstat(fd, function (err, stats) {
                        var bufferSize = stats.size,
                            buffer = new Buffer(bufferSize);
                        console.log(netpath + " def " + bufferSize);
                        fs.readSync(fd, buffer, 0, bufferSize, 0);
                        console.log(buffer);
                    });
                    fs.close(fd);
                }); */
            });
            }
        });
        res.redirect('/');
}

exports.eco = function (req, res) {
    var changeLoc;
    var changeType;
    var description;
    var wholenumber = req.params.id;
    var revNum = 0;
    var data = JSON.parse(req.body.dataPasser);
    var connection = mysql.createConnection({
        host: 'localhost',
        user: 'root',
        password: 'tfowifd',
        database: 'scandb'
    });
    connection.query("SELECT pnid FROM Basic WHERE arena = " + wholenumber.substring(0, 9), function (err, results) {
        var basic_pnid = results[0].pnid;
        for (var i = 0; i < data.length; i++) {
            changeLoc = data[i].changeLoc;
            changeType = data[i].changeType;
            if (changeLoc === "Bom" && changeType === "Add")
                description = "Value: " + data[i].value + ", Reference : " + data[i].refDes;
            else if (changeLoc === "Bom" && changeType === "Change") {
                if (data[i].newRefDes === "" && data[i].newValue === "") {
                    changeType = "Remove";
                    description = "Value: " + data[i].currentValue + ", Reference: " + data[i].currentRefDes;
                }
                else
                    description = "OldValue: " + data[i].currentValue + ", OldReference: " + data[i].currentRefDes + ", NewValue: " + data[i].newValue + ", NewReference: " + data[i].newRefDes;
            }
            else
                description = "NetName: " + data[i].netName + ", NodeName: " + data[i].nodeName + ", Reference: " + data[i].refId + ", Pin: " + data[i].pin;
            var post = { basic_pnid: basic_pnid, revNum: revNum, changeLoc: changeLoc, changeType: changeType, description: description };
            connection.query("INSERT INTO Eco SET ?", post);
        }
        res.redirect('/view/' + wholenumber);
    });
}

exports.changerev = function (req, res) {
    var wholenumber = req.params.id;
    var revNum = Number(req.body.revPasser);
    var idData = JSON.parse(req.body.idPasser);
    var connection = mysql.createConnection({
        host: 'localhost',
        user: 'root',
        password: 'tfowifd',
        database: 'scandb'
    });
        for (var i = 0; i < idData.length; i++) {
            var inserts = [revNum, idData[i]];
            var sql = "UPDATE Eco SET revNum = ? WHERE ecoid = ?";
            sql = mysql.format(sql, inserts);
            console.log(sql);
            var query = connection.query(sql);
        }
        res.redirect('/view/' + wholenumber);
}