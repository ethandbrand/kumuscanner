﻿var mysql = require('mysql');

exports.init = function (req, res) {
    var wholenumber = req.params.id;
    var connection = mysql.createConnection({
        host: 'localhost',
        user: 'root',
        password: 'tfowifd',
        database: 'scandb'
    });
    connection.query("SELECT buildNumber FROM Units WHERE serialNumber = " + wholenumber.substring(9), function (err, results) {
        var buildnumber = results[0].buildNumber;
        connection.query("SELECT pnid FROM Basic WHERE arena = " + wholenumber.substring(0, 9), function (err, results) {
            var basic_pnid = results[0].pnid;
            connection.query("SELECT revNum FROM Eco WHERE basic_pnid = " + basic_pnid, function (err, results) {
                if (results[0] != undefined) {
                    var index = [results[0].revNum];
                    var exist;
                    for (var i = 1; i < results.length; i++) {
                        exist = false;
                        for (var j = 0; j < index.length; j++) {
                            if (results[i].revNum === index[j])
                                exist = true;
                        }
                        if (exist === false)
                            index.push(results[i].revNum);
                    }
                }
                else
                    var index = "NONE";
                    connection.query("SELECT * FROM Eco WHERE basic_pnid = " + basic_pnid, function (err, results) {
                        res.render('viewpage', { pageData: { wholenumber: wholenumber , buildnumber: buildnumber, boxes: index, results: results } });
                });
            });
        });
    });
}

exports.netview = function (req, res) {
    var wholenumber = req.params.id;
    var connection = mysql.createConnection({
        host: 'localhost',
        user: 'root',
        password: 'tfowifd',
        database: 'scandb'
    });
    connection.query("SELECT pnid FROM Basic WHERE arena = " + wholenumber.substring(0,9), function (err, results) {
        var basic_pnid = results[0].pnid;
        //connection.query("SELECT * FROM Netlist WHERE units_bid = " + units_bid, function (err, results) {
        connection.query("SELECT * FROM Netlist WHERE basic_pnid = " + basic_pnid, function (err, results) {
            res.render('netlisttable', { pageData: { results: results , wholenumber: wholenumber } });
        });
    });
}

exports.bomview = function (req, res) {
    var wholenumber = req.params.id;
    var connection = mysql.createConnection({
        host: 'localhost',
        user: 'root',
        password: 'tfowifd',
        database: 'scandb'
    });
    connection.query("SELECT pnid FROM Basic WHERE arena = " + wholenumber.substring(0,9), function (err, results) {
        var basic_pnid = results[0].pnid;
        connection.query("SELECT * FROM Bom WHERE basic_pnid = " + basic_pnid, function (err, results) {
            res.render('bomtable', { pageData: { results: results , wholenumber: wholenumber } });
        });
    });
}