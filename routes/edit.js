﻿var mysql = require('mysql');

exports.eco = function (req, res) {
    var wholenumber = req.params.id;
    var arenanumber = wholenumber.substring(0, 9);
    var serialnumber = wholenumber.substring(9);
    res.render('editeco', { pageData: { arena: arenanumber, wholenumber: wholenumber} });
}